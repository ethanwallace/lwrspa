/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const title = 'Minimal React - Webpack - Babel';
const MOUNT_NODE = document.getElementById('app');

ReactDOM.render(
    <div>
        <h1 styleName="title">{title}</h1>
        <p styleName="text">
            This is a minimal React-Webpack-Babel configuration intended as a base for SPAs.
        </p>
        <h2 styleName="title">Dev Environment</h2>
        <p styleName="text">
            It contains a webpack dev server and babel support for ES6 syntax.
            Also threw in webpack-dashboard to make compilation more informative.
            I have added Airbnb ESLint (and <em>react recommended</em>) to ensure a consistent codebase.
            I have set the ESLint <code>max-len</code> options to be 120 characters.
            I have also added a rule to throw an error when proptypes are not correctly defined and exit compilation.
        </p>
        <p styleName="text">
            Finally, I have added babel-plugin-react-css-modules for atomic component styling.
            In this setup, please use <code>styleName=&quot;...&quot;</code> for local component styles. In the case of
            &apos;global styles&apos; (e.g., bootstrap) please use <code>className=&#123;...&#125;</code>.
        </p>
        <h2 styleName="title">Other Recommendations</h2>
        <p styleName="text">
            I recommend the &quot;Semantic UI&quot; library for your components - development speed is important!
            Don&apos;t re-invent the wheel and only use custom components where absolutely needed.
        </p>
        <p styleName="text">
            Finally there is the issue of routing and state management - these are not included in this distribution.
            I leave it up to the developer to know what is best for their specific application.
            I recommend investigating Redux, react-router, and Redux Saga.
        </p>
        <p styleName="text">
            A good SPA will usually need some sort of authentication for users, and the only two choices I would
            consider would be firebase or Passport.js (assuming you don&apos;t want/have access to AWS Cognito or
            something of that ilk.)
        </p>
    </div>,
    MOUNT_NODE,
);

module.hot.accept();
