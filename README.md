# LWRSPA - LightWeight React SPA

This is boilerplate for a lightweight SPA. I have tried to make the system as minimalist as possible - I have 
only added dependencies for things that *should* be added to any serious project anyway; for example, ESLint
configurations, webpack hot-loading, and hot-loading css modules. What I have not added is anything that
would relate to the specifics of the application itself - while I always recommend React-Router, it may not
always be the correct choice and your use-case may necessitate MobX instead.

## Specifics

### ESLINT

The default ESLint configuration uses *airbnb-base* and *react/recommended* along with a custom rule to enforce
a max line length of 120 characters. I also inserted a rule that causes webpack to fail to compile if a React
component does not have it's prop-types properly specified. This is something I think ought to be done if you're 
looking to create a professional application as it helps catch silly bugs and helps other read your code.
If you are very much agains this, you can remove the rule in `.eslintrc`.

### Webpack & Babel

The webpack config is very simple, it should be easy to read even if you're a Webpack layman. We have loaded the
hot-loader plugin and *style-loader* and *css-loader*, which we need for *babel-plugin-react-css-modules*.

I also included the *webpack-dashboard* plugin. Personally, I think this is an extremely useful tool for developers
as it displays quite a lot of information about the compilation process in one place, saving you from hunting it
down. If you don't want this functionality (I've experienced some issues rendering the dashboard on Windows)
then remove the `new dashboard()` line from **plugins**.

The `.babelrc` file is very simple, and adds react and stage-2 support to babel. Here I also have some options related
to hot-reloading css modules.