const webpack = require('webpack');
const dashboard = require('webpack-dashboard/plugin');
const path = require('path')

module.exports = {
	entry: './src/index.js',
	module: {
		rules: [{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			use: ['babel-loader', 'eslint-loader']
		}, {
			test: /\.css$/,
			include: path.resolve(__dirname, './src'),
			loaders: [
				'style-loader',
				'css-loader?importLoader=1&modules&localIdentName=[name]-[local]-[hash:base64:4]'
			]
		}]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx']
	},
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'bundle.js'
    },
	plugins: [
	    new webpack.HotModuleReplacementPlugin,
			new dashboard()
	],
    devServer: {
        contentBase: './dist',
		hot: true
    }
};